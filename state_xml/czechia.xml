<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE State SYSTEM "DocTypeDef.dtd">
<State name="Czechia">

    <Introduction>
        <Background>
            At the close of World War I, the Czechs and Slovaks of the former Austro-Hungarian Empire merged to form Czechoslovakia. During the interwar years, having rejected a federal system, the new country's predominantly Czech leaders were frequently preoccupied with meeting the increasingly strident demands of other ethnic minorities within the republic, most notably the Slovaks, the Sudeten Germans, and the Ruthenians (Ukrainians). On the eve of World War II, Nazi Germany occupied the territory that today comprises Czechia, and Slovakia became an independent state allied with Germany. After the war, a reunited but truncated Czechoslovakia (less Ruthenia) fell within the Soviet sphere of influence. In 1968, an invasion by Warsaw Pact troops ended the efforts of the country's leaders to liberalize communist rule and create "socialism with a human face," ushering in a period of repression known as "normalization." The peaceful "Velvet Revolution" swept the Communist Party from power at the end of 1989 and inaugurated a return to democratic rule and a market economy. On 1 January 1993, the country underwent a nonviolent "velvet divorce" into its two national components, the Czech Republic and Slovakia. The Czech Republic joined NATO in 1999 and the European Union in 2004. The country added the short-form name Czechia in 2016, while continuing to use the full form name, Czech Republic.
        </Background>
    </Introduction>

    <Geography>
        <Location>
            <Description>
                Central Europe
            </Description>
            <Neighboors>
                <Neighboor>Germany</Neighboor>
                <Neighboor>Poland</Neighboor>
                <Neighboor>Slovakia</Neighboor>
                <Neighboor>Austria</Neighboor>
            </Neighboors>
        </Location>
        <GeoCoordinates N="49 45" E="15 30"/>
        <MapReferences value="Europe" />

        <Area total="78867" land="77247" water="1620" units="sq km"/>
        <Coastline total="0" units="km" />
        <Terrain>
            <Description>
                Bohemia in the west consists of rolling plains, hills, and plateaus surrounded by low mountains; Moravia in the east consists of very hilly country
            </Description>
        </Terrain>
        <Elevation>
            <HighestPoint high="1602" units="m">Snezka</HighestPoint>
            <LowestPoint low="115" units="m">Labe River</LowestPoint>
            <MeanElevation units="m">433</MeanElevation>
        </Elevation>

        <NaturalResources>
            <Data>
                hard coal, soft coal, kaolin, clay, graphite, timber, arable land
            </Data>
        </NaturalResources>

        <PopulationDistribution>
            <Description>
                a fairly even distribution throughout most of the country, but the northern and eastern regions tend to have larger urban concentrations
            </Description>
        </PopulationDistribution>

    </Geography>

    <PeopleAndSociaty>
        <Population total="10702596" date="2021"/>

        <EthnicGroups units="%" date="2011">
            <EthnicGroup name="Czech" quantity="64.3" />
            <EthnicGroup name="Moravian" quantity="5" />
            <EthnicGroup name="Slovak" quantity="1.4" />
            <EthnicGroup name="Other" quantity="1.8" />
            <EthnicGroup name="Unspecified" quantity="27.5" />
        </EthnicGroups>

        <Languages units="%" date="2011">
            <Language name="Czech" quantity="95.4" />
            <Language name="Slovak" quantity="1.6" />
            <Language name="Other" quantity="3" />
        </Languages>

        <Religions units="%" date="2011">
            <Religion name="Roman Catholic" quantity="10.4" />
            <Religion name="Protestant" quantity="1.1" />
            <Religion name="Other and unspecified" quantity="54" />
            <Religion name="None" quantity="34.5" />
        </Religions>

        <AgeStructure date="2020">
            <Struct from="0" to="14">
                <Total units="%">15.17</Total>
                <Male>834447</Male>
                <Female>789328</Female>
            </Struct>
            <Struct from="15" to="24">
                <Total units="%">9.2</Total>
                <Male>508329</Male>
                <Female>475846</Female>
            </Struct>
            <Struct from="25" to="54">
                <Total units="%">43.29</Total>
                <Male>2382899</Male>
                <Female>2249774</Female>
            </Struct>
            <Struct from="55" to="64">
                <Total units="%">12.12</Total>
                <Male>636357</Male>
                <Female>660748</Female>
            </Struct>
            <Struct from="64" to="over">
                <Total units="%">20.23</Total>
                <Male>907255</Male>
                <Female>1257515</Female>
            </Struct>
        </AgeStructure>

        <MedianAge total="43.3" male="42" female="44.7" units="years"/>

        <Literacy total="99" male="99" female="99" units="%" date="2011" />

        <Unemployment total="5.6" male="5.3" female="6" units="%" date="2019" />

    </PeopleAndSociaty>

    <Environment>
        <CurrentIssues>
            air and water pollution in areas of northwest Bohemia and in northern Moravia around Ostrava present health risks; acid rain damaging forests; land pollution caused by industry, mining, and agriculture
        </CurrentIssues>

        <AirPolllutants>
            <Matter name="particulate matter emissions" units="micrograms per cubic meter" date="2016">15.15</Matter>
            <Matter name="carbon dioxide emissions" units="megatons" date="2016">102.22</Matter>
            <Matter name="methane emissions" units="megatons" date="2020">13.11</Matter>
        </AirPolllutants>

        <TotalWaterWithdrawal>
            <Withdrawal purpose="municipal" units="million cubic meters" date="2017">616.6</Withdrawal>
            <Withdrawal purpose="industrial" units="million cubic meters" date="2017">967.2</Withdrawal>
            <Withdrawal purpose="agricultural" units="million cubic meters" date="2017">46.6</Withdrawal>
        </TotalWaterWithdrawal>

        <TotalRenewableWaterResources total="13.15" units="billion cubic meters" date="2017" />

        <LandUse>
            <Usecase name="agricultural land" units="%" date="2018">54.8</Usecase>
            <Usecase name="forest" units="%" date="2018">34.4</Usecase>
            <Usecase name="other" units="%" date="2018">10.8</Usecase>
        </LandUse>

    </Environment>

    <Energy>
        <Electricity>
            <Access units="%" date="2020">100</Access>
            <Production units="billion kWh" date="2016">77.39</Production>
            <Consumption units="billion kWh" date="2016">62.34</Consumption>
            <Exports units="billion kWh" date="2016">24.79</Exports>
            <Imports units="billion kWh" date="2016">13.82</Imports>
            <FromFossilFuels units="%" date="2016">60</FromFossilFuels>
            <FromNuclearFuels units="%" date="2017">19</FromNuclearFuels>
            <FromHydroelectricPlants units="%" date="2017">5</FromHydroelectricPlants>
            <FromOtherSources units="%" date="2017">16</FromOtherSources>
        </Electricity>
        <CrudeOil>
            <Production units="bbl/day" date="2018">2000</Production>
            <Exports units="bbl/day" date="2017">446</Exports>
            <Imports units="bbl/day" date="2017">155900</Imports>
            <ProvedReserves units="million bbl" date="2018">15</ProvedReserves>
        </CrudeOil>
        <RefinedPetroleum>
            <Production units="bbl/day" date="2017">177500</Production>
            <Consumption units="bbl/day" date="2017">213700</Consumption>
            <Exports units="bbl/day" date="2017">52200</Exports>
            <Imports units="bbl/day" date="2017">83860</Imports>
        </RefinedPetroleum>
        <NaturalGas>
            <Production units="million cu m" date="2017">229.4</Production>
            <Consumption units="billion cu m" date="2017">8.721</Consumption>
            <Exports units="cu m" date="2017">0</Exports>
            <Imports units="billion cu m" date="2017">8.891</Imports>
            <ProvedReserves units="billion cu m" date="2018">3.964</ProvedReserves>
        </NaturalGas>
    </Energy>

    <Communications>
        <Telephones>
            <FixedLines>
                <TotalSubscriptions>1473846</TotalSubscriptions>
                <Subscriptions units="per 100 inhabitants" date="2019">13.78</Subscriptions>
            </FixedLines>
            <MobileCellular>
                <TotalSubscriptions>13213279</TotalSubscriptions>
                <Subscriptions units="per 100 inhabitants" date="2019">123.54</Subscriptions>
            </MobileCellular>

        </Telephones>
        <TelecommuniationSystems>
            <GeneralAssessment>
                good telephone and Internet service; the Czech Republic has a sophisticated telecom market; mobile sector showing steady growth, but perhaps without enough competition, regulator makes progress for 5G services; the govt. trying to stimulate competition, improve end-users pricing and step up quality; strong growth in cable and fiber sectors; fixed wireless broadband remains strong, with penetration among the highest in the EU (2020)
            </GeneralAssessment>
            <Domestic>
                14 per 100 fixed-line and mobile telephone usage increased to 124 per 100 mobile-cellular, the number of cellular telephone subscriptions now greatly exceeds the population (2019)
            </Domestic>
            <International>
                country code - 420; satellite earth stations - 6 (2 Intersputnik - Atlantic and Indian Ocean regions, 1 Intelsat, 1 Eutelsat, 1 Inmarsat, 1 Globalstar) (2019)
            </International>
        </TelecommuniationSystems>
        <BroadcastMedia>
            22 TV stations operate nationally, with 17 of them in private hands; publicly operated Czech Television has 5 national channels; throughout the country, there are some 350 TV channels in operation, many through cable, satellite, and IPTV subscription services; 63 radio broadcasters are registered, operating over 80 radio stations, including 7 multiregional radio stations or networks; publicly operated broadcaster Czech Radio operates 4 national, 14 regional, and 4 Internet stations; both Czech Radio and Czech Television are partially financed through a license fee (2019)
        </BroadcastMedia>
        <Internet countryCode=".cz">
            <InternetUsers total="8622750" percentOfPopulation="80.69%" date="2018" />
        </Internet>
        <Broadband>
            <FixedSubscriptions total="3222835" per100inhabitants="30" date="2018"/>
        </Broadband>
    </Communications>

    <Transportation>
        <Airports total="128" date="2013">
            <WithPavedRunways total="41" date="2017">
                <Runway from="3047" to="up" date="2017">2</Runway>
                <Runway from="2438" to="3047 " date="2017">9</Runway>
                <Runway from="1524 " to="2437" date="2017">12</Runway>
                <Runway from="914 " to="1523" date="2017">2</Runway>
                <Runway from="0 " to="914" date="2017">16</Runway>
            </WithPavedRunways>
            <WithUnpavedRunways total="87" date="2013">
                <Runway from="1524 " to="2437" date="2013">1</Runway>
                <Runway from="914 " to="1523" date="2013">25</Runway>
                <Runway from="0 " to="914" date="2013">61</Runway>
            </WithUnpavedRunways>
        </Airports>
        <Heliports date="2013">
            1
        </Heliports>
        <Pipelines units="km" date="2016">
            <Gas>7160</Gas>
            <Oil>675</Oil>
            <RefinedProducts>94</RefinedProducts>
        </Pipelines>
        <Railways total="9408" units="km" date="2017">
            <StandardGauge>
                9385
            </StandardGauge>
            <NarrowGauge>
                23
            </NarrowGauge>
        </Railways>
        <RoadWays total="55744" units="km" date="2019">
            <Paved>
                55744
            </Paved>
        </RoadWays>
        <Waterways total="664" units="km" date="2010">
            <Description>
                principally on Elbe, Vltava, Oder, and other navigable rivers, lakes, and canals
            </Description>
        </Waterways>
        <PortsAndTerminals>
            <RiverPort>
                Prague (Vltava), Decin, Usti nad Labem (Elbe)
            </RiverPort>
        </PortsAndTerminals>
    </Transportation>

    <MilitaryAndSecurity>
        <MilitaryExpenditures>
            <Year date="2020" units="% of GDP">1.34</Year>
            <Year date="2019" units="% of GDP">1.16</Year>
            <Year date="2018" units="% of GDP">1.13</Year>
            <Year date="2017" units="% of GDP">1.04</Year>
            <Year date="2016" units="% of GDP">0.96</Year>
        </MilitaryExpenditures>
        <MilitaryStrengths date="2020">
            the Czech military has approximately 26,000 active personnel (20,000 Army; 6,000 Air Force)
        </MilitaryStrengths>
        <MilitaryEquipment date="2020">
            the Czech military has a mix of Soviet-era and more modern equipment, mostly of European origin; since 2010, the leading suppliers of military equipment to Czechia are Austria and Spain
        </MilitaryEquipment>
        <MilitaryServiceAge date="2004">
            18-28 years of age for male and female voluntary military service; conscription abolished
        </MilitaryServiceAge>
    </MilitaryAndSecurity>

    <TransnationalIssues>
        <Disputes>
            none
        </Disputes>
        <InternallyDisplacedPersons>
            <StatelessPersons date="2019">1394</StatelessPersons>
        </InternallyDisplacedPersons>
        <IllicitDrugs>
            transshipment point for Southwest Asian heroin and minor transit point for Latin American cocaine to Western Europe; producer of synthetic drugs for local and regional markets; susceptible to money laundering related to drug trafficking, organized crime; significant consumer of ecstasy
        </IllicitDrugs>
    </TransnationalIssues>

</State>