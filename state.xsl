<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html lang="en">
            <head>
                <link rel="stylesheet" href="../style/main_style.css" />
            </head>
            <body>
                <section class="top_section">
                    <navbar>
                        <h1><a href="../index.html">The Fact Book</a></h1>
                    </navbar>
                </section>
                <div class="grid-container">
                    <div class="grid-item">
                        <navbar class="nav_part">
                            <ol>
                                <a href="#Introduction"><li>Introduction</li></a>
                                <a href="#Geography"><li>Geography</li></a>
                                <a href="#PeopleAndSociaty"><li>People and Society</li></a>
                                <a href="#Environment"><li>Environment</li></a>
                                <a href="#Energy"><li>Energy</li></a>
                                <a href="#Communications"><li>Communications</li></a>
                                <a href="#Transportation"><li>Transportation</li></a>
                                <a href="#MilitaryAndSecurity"><li>Military and Security</li></a>
                                <a href="#TransnationalIssues"><li>Transnational issues</li></a>
                            </ol>
                        </navbar>
                    </div>
                    <div class="grid-item main_part">
                        <h2>
                            <xsl:value-of select="State/@name" />
                        </h2>
                        <xsl:apply-templates/>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="Introduction">
        <section id="Introduction">
        <h3>
            Introduction
        </h3>
        <h4>
            Background
        </h4>
        <p>
            <xsl:value-of select="./Background" />
        </p>
        </section>
    </xsl:template>

    <xsl:template match="Geography">
        <section id="Geography">
        <h3>
            Geography
        </h3>
        <h4>
            Location
        </h4>
        <p>
            <xsl:value-of select="./Location/Description" />
            <xsl:choose>
                <xsl:when test="./Location/Neighboors/Neighboor">
                    between
                    <xsl:for-each select="./Location/Neighboors/Neighboor">
                        <xsl:value-of select="."/><xsl:text> </xsl:text>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>

        </p>
        <h4>Geographic coordinates</h4>
        <p>
            <xsl:value-of select="./GeoCoordinates/@N" />
            <xsl:text> N, </xsl:text>
            <xsl:value-of select="./GeoCoordinates/@E" />
            <xsl:text> E</xsl:text>
        </p>
        <h4>Map references</h4>
        <p>
            <xsl:value-of select="./MapReferences/@value" />
        </p>
        <h4>Area</h4>
        <p>
            <table>
                <tr>
                    <td>Total: </td>
                    <td><xsl:value-of select="./Area/@total" /><xsl:text> </xsl:text><xsl:value-of select="./Area/@units" /></td>
                </tr>
                <tr>
                    <td>Land: </td>
                    <td><xsl:value-of select="./Area/@land" /><xsl:text> </xsl:text><xsl:value-of select="./Area/@units" /></td>
                </tr>
                <tr>
                    <td>Water: </td>
                    <td><xsl:value-of select="./Area/@water" /><xsl:text> </xsl:text><xsl:value-of select="./Area/@units" /></td>
                </tr>
            </table>
        </p>
        <h4>
            Coastline
        </h4>
        <p>
            <xsl:value-of select="./Coastline/@total" /><xsl:text> </xsl:text><xsl:value-of select="./Coastline/@units" />
        </p>
        <h4>
            Terrain
        </h4>
        <p>
            <xsl:value-of select="./Terrain/Description" />
        </p>
        <h4>
            Elevation
        </h4>
        <p>
            <table>
                <tr>
                    <td>Highest point: </td>
                    <td><xsl:value-of select="./Elevation/HighestPoint" /><xsl:text> </xsl:text><xsl:value-of select="./Elevation/HighestPoint/@high" /><xsl:text> </xsl:text><xsl:value-of select="./Elevation/HighestPoint/@units" /></td>
                </tr>
                <tr>
                    <td>Lowest point: </td>
                    <td><xsl:value-of select="./Elevation/LowestPoint" /><xsl:text> </xsl:text><xsl:value-of select="./Elevation/LowestPoint/@low" /><xsl:text> </xsl:text><xsl:value-of select="./Elevation/LowestPoint/@units" /></td>
                </tr>
                <tr>
                    <td>Mean elevation: </td>
                    <td><xsl:value-of select="./Elevation/MeanElevation" /><xsl:text> </xsl:text><xsl:value-of select="./Elevation/MeanElevation/@units" /></td>
                </tr>
            </table>
        </p>

        <h4>
            Natural Resources
        </h4>
        <p>
            <xsl:value-of select="./NaturalResources/Data" />
        </p>
        <h4>
            Population distribution
        </h4>
        <xsl:value-of select="./PopulationDistribution/Description" />
        </section>
    </xsl:template>

    <xsl:template match="PeopleAndSociaty">
        <section id="PeopleAndSociaty">
        <h3>
            People and Society
        </h3>
        <h4>
            Population
        </h4>
        <p>
            <xsl:value-of select="./Population/@total" />
            <xsl:text> </xsl:text>
            (<xsl:value-of select="./Population/@date" />)
        </p>
        <h4>
            Ethnic groups
        </h4>
        <table>
            <xsl:for-each select="./EthnicGroups/EthnicGroup">
                <tr>
                    <td><xsl:value-of select="./@name" /></td>
                    <td><xsl:value-of select="./@quantity" /> <xsl:value-of select="../@units"/></td>
                </tr>
            </xsl:for-each>
            <tr>
                <td>Date</td>
                <td><xsl:value-of select="./EthnicGroups/@date" /></td>
            </tr>
        </table>
        <h4>
            Languages
        </h4>
        <table>
            <xsl:for-each select="./Languages/Language">
                <tr>
                    <td><xsl:value-of select="./@name" /></td>
                    <td><xsl:value-of select="./@quantity" /> <xsl:value-of select="../@units"/></td>
                </tr>
            </xsl:for-each>
            <tr>
                <td>Date</td>
                <td><xsl:value-of select="./Languages/@date" /></td>
            </tr>
        </table>
        <h4>
            Religions
        </h4>
        <table>
            <xsl:for-each select="./Religions/Religion">
                <tr>
                    <td><xsl:value-of select="./@name" /></td>
                    <td><xsl:value-of select="./@quantity" /> <xsl:value-of select="../@units"/></td>
                </tr>
            </xsl:for-each>
            <tr>
                <td>Date</td>
                <td><xsl:value-of select="./Religions/@date" /></td>
            </tr>
        </table>
        <h4>
            Age structure
        </h4>
        <table>
            <xsl:for-each select="./AgeStructure/Struct">
                <tr>
                    <td><xsl:value-of select="./@from" /> - <xsl:value-of select="./@to" /></td>
                    <td><xsl:value-of select="./Total" /> <xsl:value-of select="./Total/@units" /> (Male <xsl:value-of
                            select="./Male" /> / Female <xsl:value-of select="./Female" />)</td>
                </tr>
            </xsl:for-each>
            <tr>
                <td>Date</td>
                <td><xsl:value-of select="./AgeStructure/@date" /></td>
            </tr>
        </table>
        <h4>
            Median Age
        </h4>
        <table>
            <tr>
                <td>Total</td>
                <td><xsl:value-of select="./MedianAge/@total" /> <xsl:value-of select="./MedianAge/@units"/></td>
            </tr>
            <tr>
                <td>Male</td>
                <td><xsl:value-of select="./MedianAge/@male" /> <xsl:value-of select="./MedianAge/@units"/></td>
            </tr>
            <tr>
                <td>Female</td>
                <td><xsl:value-of select="./MedianAge/@female" /> <xsl:value-of select="./MedianAge/@units"/></td>
            </tr>
        </table>
        <h4>
            Literacy
        </h4>
        <table>
            <tr>
                <td>Total</td>
                <td><xsl:value-of select="./Literacy/@total" /> <xsl:value-of select="./Literacy/@units"/></td>
            </tr>
            <tr>
                <td>Male</td>
                <td><xsl:value-of select="./Literacy/@male" /> <xsl:value-of select="./Literacy/@units"/></td>
            </tr>
            <tr>
                <td>Female</td>
                <td><xsl:value-of select="./Literacy/@female" /> <xsl:value-of select="./Literacy/@units"/></td>
            </tr>
            <tr>
                <td>Date</td>
                <td><xsl:value-of select="./Literacy/@date" /></td>
            </tr>
        </table>
        <h4>
            Unemployment
        </h4>
        <table>
            <tr>
                <td>Total</td>
                <td><xsl:value-of select="./Unemployment/@total" /> <xsl:value-of select="./Unemployment/@units"/></td>
            </tr>
            <tr>
                <td>Male</td>
                <td><xsl:value-of select="./Unemployment/@male" /> <xsl:value-of select="./Unemployment/@units"/></td>
            </tr>
            <tr>
                <td>Female</td>
                <td><xsl:value-of select="./Unemployment/@female" /> <xsl:value-of select="./Unemployment/@units"/></td>
            </tr>
            <tr>
                <td>Date</td>
                <td><xsl:value-of select="./Unemployment/@date" /></td>
            </tr>
        </table>
        </section>
    </xsl:template>

    <xsl:template match="Environment">
        <section id="Environment">
        <h3>
            Environment
        </h3>
        <h4>
            Current issues
        </h4>
        <p>
            <xsl:value-of select="./CurrentIssues"></xsl:value-of>
        </p>
        <h4>
            Air polllutants
        </h4>
        <table>
            <xsl:for-each select="./AirPolllutants/Matter">
                <tr>
                    <td><xsl:value-of select="./@name" /></td>
                    <td><xsl:value-of select="." /> <xsl:value-of select="./@units" /> (<xsl:value-of select="./@date" />)</td>
                </tr>
            </xsl:for-each>
        </table>
        <h4>
            Total water withdrawal
        </h4>
        <table>
            <xsl:for-each select="./TotalWaterWithdrawal/Withdrawal">
                <tr>
                    <td><xsl:value-of select="./@purpose" /></td>
                    <td><xsl:value-of select="." /> <xsl:value-of select="./@units" /> (<xsl:value-of select="./@date" />)</td>
                </tr>
            </xsl:for-each>
        </table>
        <h4>
            Total renewable water resources
        </h4>
        <p>
            <xsl:value-of select="./TotalRenewableWaterResources/@total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="./TotalRenewableWaterResources/@units" />
            <xsl:text> </xsl:text>
            (<xsl:value-of select="./TotalRenewableWaterResources/@date" />)
        </p>
        <h4>
            Land use
        </h4>
        <table>
            <xsl:for-each select="./LandUse/Usecase">
                <tr>
                    <td><xsl:value-of select="./@name" /></td>
                    <td><xsl:value-of select="." /> <xsl:value-of select="./@units" /> (<xsl:value-of select="./@date" />)</td>
                </tr>
            </xsl:for-each>
        </table>
        </section>
    </xsl:template>

    <xsl:template match="Energy">
        <section id="Energy">
        <h3>
            Energy
        </h3>
        <h4>
            Electricity
        </h4>
        <ul>
            <xsl:for-each select="./Electricity/*">
                <li>
                    <xsl:value-of select="name(.)" />
                    <p>
                        <xsl:value-of select="." />
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="./@units" />
                        <xsl:text> </xsl:text>
                        (<xsl:value-of select="./@date" />)
                    </p>
                </li>
            </xsl:for-each>
        </ul>
        <h4>
            Crude oil
        </h4>
        <ul>
            <xsl:for-each select="./CrudeOil/*">
                <li>
                    <xsl:value-of select="name(.)" />
                    <p>
                        <xsl:value-of select="." />
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="./@units" />
                        <xsl:text> </xsl:text>
                        (<xsl:value-of select="./@date" />)
                    </p>
                </li>
            </xsl:for-each>
        </ul>
        <h4>
            Refined petroleum
        </h4>
        <ul>
            <xsl:for-each select="./RefinedPetroleum/*">
                <li>
                    <xsl:value-of select="name(.)" />
                    <p>
                        <xsl:value-of select="." />
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="./@units" />
                        <xsl:text> </xsl:text>
                        (<xsl:value-of select="./@date" />)
                    </p>
                </li>
            </xsl:for-each>
        </ul>
        <h4>
            Natural gas
        </h4>
        <ul>
            <xsl:for-each select="./NaturalGas/*">
                <li>
                    <xsl:value-of select="name(.)" />
                    <p>
                        <xsl:value-of select="." />
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="./@units" />
                        <xsl:text> </xsl:text>
                        (<xsl:value-of select="./@date" />)
                    </p>
                </li>
            </xsl:for-each>
        </ul>
        </section>
    </xsl:template>

    <xsl:template match="Communications">
        <section id="Communications">
        <h3>
            <xsl:value-of select="name(.)" />
        </h3>
        <h4>
            Telephones
        </h4>
                <xsl:for-each select="./Telephones/*">
                    <ul>
                        <li>
                            <xsl:value-of select="name(.)" />
                            <table>
                                <tr>
                                    <td>
                                        Total subscriptions
                                    </td>
                                    <td>
                                        <xsl:value-of select="./TotalSubscriptions" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Subscriptions <xsl:value-of select="./Subscriptions/@units" />
                                    </td>
                                    <td>
                                        <xsl:value-of select="./Subscriptions" />
                                        (<xsl:value-of select="./Subscriptions/@date" />)
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>

                </xsl:for-each>
        <h4>
            Telecommunication systems
        </h4>
        <table>
            <xsl:for-each select="./TelecommuniationSystems/*">
                <tr>
                    <td>
                        <xsl:value-of select="name(.)" />
                    </td>
                    <td>
                        <xsl:value-of select="." />
                    </td>
                </tr>
            </xsl:for-each>

        </table>
        <h4>
            Broadcast media
        </h4>
        <p>
            <xsl:value-of select="./BroadcastMedia" />
        </p>
        <h4>
            Internet
        </h4>
        <ul>
            <li>
                Country code
            </li>
            <p>
                <xsl:value-of select="./Internet/@countryCode" />
            </p>
            <li>
                Internet users
                <table>
                    <tr>
                        <td>
                            Total
                        </td>
                        <td>
                            <xsl:value-of select="./Internet/InternetUsers/@total" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Percent of population
                        </td>
                        <td>
                            <xsl:value-of select="./Internet/InternetUsers/@percentOfPopulation" />
                            <xsl:text> </xsl:text>
                            (<xsl:value-of select="./Internet/InternetUsers/@date" />)
                        </td>
                    </tr>
                </table>
            </li>
        </ul>

        <h4>
            Broadband - fixed subscriptions
        </h4>
        <table>
            <tr>
                <td>
                    Total
                </td>
                <td>
                    <xsl:value-of select="./Broadband/FixedSubscriptions/@total" />
                </td>
            </tr>
            <tr>
                <td>
                    Per 100 inhabitants
                </td>
                <td>
                    <xsl:value-of select="./Broadband/FixedSubscriptions/@per100inhabitants" />
                    <xsl:text> </xsl:text>
                    (<xsl:value-of select="./Broadband/FixedSubscriptions/@date" />)
                </td>
            </tr>
        </table>
        </section>
    </xsl:template>

    <xsl:template match="Transportation">
        <section id="Transportation">
        <h3>
            Transportation
        </h3>
        <h4>
            Airports
        </h4>
        <table>
            <tr>
                <td>
                    Total
                </td>
                <td>
                    <xsl:value-of select="./Airports/@total"/>
                    <xsl:text> </xsl:text>
                    (<xsl:value-of select="./Airports/@date" />)
                </td>
            </tr>
        </table>
        <ul>
            <xsl:for-each select="./Airports/*">
                <li>
                    <xsl:value-of select="name(.)" />
                    <table>
                        <xsl:for-each select="./*">
                            <tr>
                                <td>
                                    <xsl:value-of select="./@from" /> - <xsl:value-of select="./@to" />
                                </td>
                                <td>
                                    <xsl:value-of select="." />
                                    <xsl:text> </xsl:text>
                                    (<xsl:value-of select="./@date" />)
                                </td>
                            </tr>
                        </xsl:for-each>
                    </table>

                </li>
            </xsl:for-each>
        </ul>

        <h4>
            Heliports
        </h4>
        <p>
            <xsl:value-of select="./Heliports" />
            <xsl:text> </xsl:text>
            (<xsl:value-of select="./Heliports/@date" />)
        </p>

        <h4>
            Pipelines
        </h4>
        <ul>
            <xsl:for-each select="./Pipelines/*">
                <li>
                    <xsl:value-of select="name(.)" />
                    <p>
                        <xsl:value-of select="." />
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="../@units" />
                    </p>

                </li>
            </xsl:for-each>
        </ul>

        <h4>
            Railways
        </h4>
        <table>
            <tr>
                <td>
                    Total
                </td>
                <td>
                    <xsl:value-of select="./Railways/@total" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="./Railways/@units" />
                    <xsl:text> </xsl:text>
                    (<xsl:value-of select="./Railways/@date" />)
                </td>
            </tr>
            <tr>
                <td>
                    Standard gauge
                </td>
                <td>
                    <xsl:value-of select="./Railways/StandardGauge" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="./Railways/@units" />
                </td>
            </tr>
            <tr>
                <td>
                    Narrow gauge
                </td>
                <td>
                    <xsl:value-of select="./Railways/NarrowGauge" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="./Railways/@units" />
                </td>
            </tr>
        </table>

        <h4>
            Roadways
        </h4>
        <table>
            <tr>
                <td>
                    Total
                </td>
                <td>
                    <xsl:value-of select="./RoadWays/@total" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="./RoadWays/@units" />
                    <xsl:text> </xsl:text>
                    (<xsl:value-of select="./RoadWays/@date" />)
                </td>
            </tr>
            <tr>
                <td>
                    Paved
                </td>
                <td>
                    <xsl:value-of select="./RoadWays/Paved" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="./RoadWays/@units" />
                </td>
            </tr>
        </table>

        <h4>
            Waterways
        </h4>
        <xsl:value-of select="./Waterways/@total" />
        <xsl:text> </xsl:text>
        <xsl:value-of select="./Waterways/@units" />
        <xsl:text> </xsl:text>
        (<xsl:value-of select="./Waterways/Description" />)
        <xsl:text> </xsl:text>
        (<xsl:value-of select="./Waterways/@date" />)

        <h4>
            Ports and terminals
        </h4>
        <table>
            <tr>
                <td>
                    River port(s):
                </td>
                <td>
                    <xsl:value-of select="./PortsAndTerminals/RiverPort" />
                </td>
            </tr>
        </table>
        </section>
    </xsl:template>

    <xsl:template match="MilitaryAndSecurity">
        <section id="MilitaryAndSecurity">
        <h3>
            Military and Security
        </h3>
        <h4>
            Military expenditures
        </h4>
        <table>
            <xsl:for-each select="./MilitaryExpenditures/*">
                <tr>
                    <td>
                        <xsl:value-of select="./@date" />
                    </td>
                    <td>
                        <xsl:value-of select="."/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="./@units"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>

        <h4>
            Military and security service personnel strengths
        </h4>
        <p>
            <xsl:value-of select="./MilitaryStrengths" />
            <xsl:value-of select="./MilitaryStrengths/@date" />
        </p>

        <h4>
            Military equipment inventories and acquisitions
        </h4>
        <p>
            <xsl:value-of select="./MilitaryEquipment" />
            <xsl:value-of select="./MilitaryEquipment/@date" />
        </p>

        <h4>
            Military service age and obligation
        </h4>
        <p>
            <xsl:value-of select="./MilitaryServiceAge" />
            <xsl:value-of select="./MilitaryServiceAge/@date" />
        </p>
        </section>
    </xsl:template>

    <xsl:template match="TransnationalIssues">
        <section id="TransnationalIssues">
        <h3>
            Transnational issues
        </h3>
        <h4>
            Disputes
        </h4>
        <p>
            <xsl:value-of select="./Disputes" />
        </p>
        <h4>
            Refugees and internally displaced persons
        </h4>
        <table>
            <tr>
                <td>Stateless persons</td>
                <td>
                    <xsl:value-of select="./InternallyDisplacedPersons/StatelessPersons" />
                    <xsl:text> </xsl:text>
                    (<xsl:value-of select="./InternallyDisplacedPersons/StatelessPersons/@date" />)
                </td>
            </tr>
        </table>
        <h4>
            Illicit drugs
        </h4>
        <p>
            <xsl:value-of select="./IllicitDrugs" />
        </p>
        </section>
    </xsl:template>

</xsl:stylesheet>