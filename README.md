# XML Projekt

## Vytvořeno s použitím
 * PyCharm
 * Pip
 * lxml knihovny pro praci s xml v programovacim jazyce Python

## Instrukce
 * Po spuštění programu main.py lze zadávat libovolne příkazy z níže uvedeného seznamu generující odpovidající výstup
 * Prohližení jednotlivých stránek je zprostředkováno souborem index.html, v němž se na ně odkazuje

## Příkazy
 * validate xml with dtd - zvaliduje 4 xml soubory ze složky state pomoci DTD schematu ze souboru DocTypeDef.dtd
 * validate xml with rng - zvaliduje 4 xml soubory ze složky state pomoci RelaxNG schematu ze souboru RelaxNG.rng
 * validate xml - kombinace validate xml with dtd a validate xml with rng
 * check xml well-formed - kontrola, jestli jsou xml soubory well-formed
 * generate xhtml files - generuje xhtml soubory z xml souborů do složky result_xhtml, po tomto prikaze dojde k automatickemu ukonceni programu
 * exit - ukonceni programu