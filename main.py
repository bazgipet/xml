import lxml.html
from lxml import etree
import os

parser = etree.XMLParser(dtd_validation=True)


theDTD = os.path.dirname(os.path.realpath(__file__))+'/DocTypeDef.dtd'
theXMLCzechia = os.path.dirname(os.path.realpath(__file__))+'/state_xml/czechia.xml'
theXMLGermany = os.path.dirname(os.path.realpath(__file__))+'/state_xml/germany.xml'
theXMLAustria = os.path.dirname(os.path.realpath(__file__))+'/state_xml/austria.xml'
theXMLSlovakia = os.path.dirname(os.path.realpath(__file__))+'/state_xml/slovakia.xml'
theRelaxNG = os.path.dirname(os.path.realpath(__file__))+'/RelaxNG.rng'
theXSL = os.path.dirname(os.path.realpath(__file__))+'/state.xsl'
theXMLArr = [theXMLCzechia, theXMLGermany, theXMLAustria, theXMLSlovakia]

print(theDTD)
print(theXMLCzechia)
print(theXMLGermany)
print(theXMLAustria)
print(theXMLSlovakia)
print(theRelaxNG)
print(theXSL)


def validateDTD():
    dtd = etree.DTD(open(theDTD))

    for theXML in theXMLArr:
        try:
            root = etree.parse(open(theXML))
        except:
            print("XML file is not well-formed :/")
            return 0

        dtd_bool = dtd.validate(root)
        print("\nDTD validation for " + theXML + ": " + str(dtd_bool) + "\n")

        if not dtd_bool:
            print(dtd.error_log.filter_from_errors()[0])

    return 0


def validateRNG():

    relaxng_doc = etree.parse(open(theRelaxNG))
    relaxng = etree.RelaxNG(relaxng_doc)

    for theXML in theXMLArr:
        try:
            root = etree.parse(open(theXML))
        except:
            print("XML file is not well-formed :/")
            return 0

        relaxng_bool = relaxng.validate(root)
        print("\nRelaxNG validation for " + theXML + ": " + str(relaxng_bool) + "\n")

        if not relaxng_bool:
            log = relaxng.error_log
            print(log.last_error)

    return 0


def generateXHTML():

    xslt_doc = etree.parse(theXSL)
    xslt_transformer = etree.XSLT(xslt_doc)

    for theXML in theXMLArr:

        source_doc = etree.parse(theXML)
        output_doc = xslt_transformer(source_doc)

        txt = theXML
        x = txt.split("/")
        x = x[len(x) - 1].split(".")
        res = x[0]
        res = os.path.dirname(os.path.realpath(__file__))+"/result_xhtml/" + res + ".html"
        print(res)
        output_doc.write(res, pretty_print=True)
    return 0


def checkWellFormed():

    for theXML in theXMLArr:
        try:
            root = etree.parse(open(theXML))
        except:
            print(theXML + " is not well-formed :/")
            return 3

    return 0

while 1:

    command = input("Write your command: ")

    if command == "validate xml with dtd":

        validateDTD()
        continue

    if command == "validate xml with rng":

        validateRNG()
        continue

    if command == "validate xml":

        validateDTD()
        validateRNG()
        continue

    if command == "check xml well-formed":

        retVal = checkWellFormed()
        if retVal == 0:
            print("XML files is well-formed :)")
        continue

    if command == "generate xhtml files":

        generateXHTML()
        break

    if command == "exit":

        print("\nGoodbye :)")
        break

    print("incorrect command")








